/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Used to forward shortcut commands to the main-command.
 * Created by tr808axm on 13.04.2016.
 */
public class ProxiedCommandExecutor implements CommandExecutor {
    private final Command command;
    private final String subCommand;

    public ProxiedCommandExecutor(Command command, String subCommand) {
        this.command = command;
        this.subCommand = subCommand;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command oldCommand, String label, String[] args) {
        String[] newArgs = new String[args.length + 1];
        newArgs[0] = subCommand;
        System.arraycopy(args, 0, newArgs, 1, args.length);
        return command.execute(sender, command.getLabel(), newArgs);
    }
}
