/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.NumberConversions;

import java.util.HashMap;
import java.util.Map;

/**
 * Stores all data for a Location, but does not need world as World.
 * Used to store Locations when worlds have not been loaded yet.
 * Created by tr808axm on 20.09.2016.
 */
public class UnlinkedLocation implements Cloneable, ConfigurationSerializable {
    private static final String DEFAULT_WORLD_PLACEHOLDER = "{DEFAULTWORLD}";

    public static class LinkedLocationNotAvailableException extends RuntimeException {
        private LinkedLocationNotAvailableException(Throwable cause) {
            super("specified world is currently not loaded", cause);
        }
    }

    private final String world;
    private final double x;
    private final double y;
    private final double z;
    private final float yaw;
    private final float pitch;

    public UnlinkedLocation(double x, double y, double z) {
        this(DEFAULT_WORLD_PLACEHOLDER, x, y, z);
    }

    public UnlinkedLocation(String world, double x, double y, double z) {
        this(world, x, y, z, 0.0F, 0.0F);
    }

    public UnlinkedLocation(double x, double y, double z, float yaw, float pitch) {
        this(DEFAULT_WORLD_PLACEHOLDER, x, y, z, yaw, pitch);
    }

    public UnlinkedLocation(String world, double x, double y, double z, float yaw, float pitch) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public String getWorld() {
        return world;
    }

    public World getLinkedWorld() {
        return DEFAULT_WORLD_PLACEHOLDER.equals(world) ? Bukkit.getWorlds().get(0) : Bukkit.getWorld(world);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float getYaw() {
        return yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public Location getLinkedLocation() throws LinkedLocationNotAvailableException {
        World linkedWorld = getLinkedWorld();
        if (linkedWorld == null)
            throw new LinkedLocationNotAvailableException(new NullPointerException("world '" + world + "' is null/not loaded"));
        return new Location(linkedWorld, x, y, z, yaw, pitch);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> serialized = new HashMap<>();
        serialized.put("world", world);
        serialized.put("x", x);
        serialized.put("y", y);
        serialized.put("z", z);
        serialized.put("yaw", yaw);
        serialized.put("pitch", pitch);
        return serialized;
    }

    public static UnlinkedLocation deserialize(Map<String, Object> serialized) {
        String world = (String) serialized.get("world");
        return new UnlinkedLocation(world,
                NumberConversions.toDouble(serialized.get("x")),
                NumberConversions.toDouble(serialized.get("y")),
                NumberConversions.toDouble(serialized.get("z")),
                NumberConversions.toFloat(serialized.get("yaw")),
                NumberConversions.toFloat(serialized.get("pitch")));
    }

    public static UnlinkedLocation fromLocation(Location linkedLocation) {
        return new UnlinkedLocation(linkedLocation.getWorld().getName(),
                linkedLocation.getX(),
                linkedLocation.getY(),
                linkedLocation.getZ(),
                linkedLocation.getYaw(),
                linkedLocation.getPitch());
    }
}
