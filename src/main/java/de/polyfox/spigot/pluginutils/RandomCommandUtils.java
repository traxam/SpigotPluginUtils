/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * A collection of check- and parse-methods that send a formatted error message when an operation fails.
 * Created by tr808axm on 17.10.2016.
 */
public class RandomCommandUtils {
    private final ChatUtil chatUtil;
    private final Map<String, CommandHelp> commandHelp;

    public RandomCommandUtils(ChatUtil chatUtil, CommandHelp[] commandHelpArray) {
        if (chatUtil == null) throw new NullPointerException("chatUtil may not be null");
        if (commandHelpArray == null || commandHelpArray.length == 0)
            throw new NullPointerException("commandHelpArray may not be null or empty");
        this.chatUtil = chatUtil;
        this.commandHelp = new HashMap<>();
        for (CommandHelp help : commandHelpArray)
            commandHelp.put(help.node, help);
    }

    public boolean checkParameterCount(CommandSender sender, int argCount, int minParameterCount, String commandHelpNode) {
        if (argCount >= minParameterCount)
            return true;
        chatUtil.send(sender, commandHelp.get(commandHelpNode).build(), true);
        return false;
    }

    public boolean checkIsPlayer(CommandSender sender) {
        if (sender instanceof Player)
            return true;
        chatUtil.send(sender, ChatUtil.NO_EXECUTION_FROM_CONSOLE, false);
        return false;
    }

    public boolean checkPermission(CommandSender sender, boolean sendNoPermMessage) {
        if (sender.isOp())
            return true;
        if (sendNoPermMessage)
            chatUtil.send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
        return false;
    }

    public UUID parseOnlinePlayerUUID(CommandSender sender, String name) {
        Player player = Bukkit.getPlayerExact(name);
        if (player != null) return player.getUniqueId();
        chatUtil.send(sender, "There is no player with the name '" + ChatUtil.VARIABLE + name + ChatUtil.NEGATIVE + "' online!", false);
        return null;
    }

    public BigDecimal parseBigDecimal(CommandSender sender, String amountString) {
        try {
            return new BigDecimal(amountString);
        } catch (NumberFormatException e) {
            chatUtil.send(sender, "'" + ChatUtil.VARIABLE + amountString + ChatUtil.NEGATIVE + "' isn't a valid number!", false);
            return null;
        }
    }
}
