/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

/**
 * A random collection of methods about players.
 * Created by tr808axm on 19.11.2016.
 */
public class PlayerUtils {
    public static void reset(Player player) {
        reset(player, true, true, true, true, true, true, true, true, true, true);
    }

    public static void reset(Player player,
                             boolean clearInv, boolean clearPotion, boolean resetHeath, boolean resetNames, boolean resetWeatherTime, boolean resetXP,
                             boolean resetGM, boolean resetHunger, boolean resetTitle, boolean resetVelocity) {
        if (clearInv) clearWholeInventory(player);
        if (clearPotion) clearAllPotionEffects(player);
        if (resetHeath) resetHealth(player);
        if (resetNames) resetNames(player);
        if (resetWeatherTime) resetCustomTimeAndWeather(player);
        if (resetXP) resetExperience(player);
        if (resetGM) player.setGameMode(Bukkit.getDefaultGameMode());
        if (resetHunger) player.setFoodLevel(20);
        if (resetTitle) player.resetTitle();
        if (resetVelocity) player.setVelocity(new Vector(0, 0, 0));
    }

    public static void resetExperience(Player player) {
        player.setTotalExperience(0);
        player.setExp(0);
        player.setLevel(0);
    }

    public static void resetCustomTimeAndWeather(Player player) {
        player.resetPlayerTime();
        player.resetPlayerWeather();
    }

    public static void resetNames(Player player) {
        player.setDisplayName(player.getName());
        player.setPlayerListName(player.getName());
        player.setCustomName(null);
        player.setCustomNameVisible(false);
    }

    public static void clearWholeInventory(Player player) {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
    }

    public static void clearAllPotionEffects(Player player) {
        for (PotionEffect effect : player.getActivePotionEffects())
            player.removePotionEffect(effect.getType());
    }

    public static void resetHealth(Player player) {
        player.resetMaxHealth();
        player.setHealth(player.getMaxHealth());
    }
}
