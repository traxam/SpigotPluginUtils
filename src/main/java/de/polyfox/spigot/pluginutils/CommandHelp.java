/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

/**
 * Represents a help topic.
 * Created by tr808axm on 17.10.2016.
 */
public class CommandHelp {
    public final String node;
    public final String baseCommand;
    public final String helpMessage;
    public final String[] commands;

    /**
     * Creates a help entry
     *
     * @param node        The key of this command.
     * @param baseCommand The title of the help menu. (Don't use aliases or parameters)
     * @param helpMessage The help message displayed next to this command.
     * @param commands    The actual command and its aliases.
     */
    public CommandHelp(String node, String baseCommand, String helpMessage, String... commands) {
        this.node = node;
        this.baseCommand = baseCommand;
        this.helpMessage = helpMessage;
        this.commands = commands;
    }

    public String[] build() {
        String[] messages = new String[5 + commands.length];
        messages[0] = "----- " + baseCommand + " | Help menu -----";
        messages[1] = "";
        messages[2] = ChatUtil.VARIABLE + helpMessage;
        messages[3] = "";
        messages[4] = ChatUtil.DETAILS + "Usage:";
        for (int i = 0; i < commands.length; i++) //Print all aliases
            messages[i + 5] = ChatUtil.DETAILS + "  " + commands[i];
        return messages;
    }
}
