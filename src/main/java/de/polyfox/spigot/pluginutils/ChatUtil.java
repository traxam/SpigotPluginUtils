/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Used to standardize chat messages. Default usage: 1 Instance per plugin.
 * Created by tr808axm on 29.04.2016.
 */
public class ChatUtil {
    public static final ChatColor POSITIVE = ChatColor.GREEN;
    public static final ChatColor NEGATIVE = ChatColor.RED;
    public static final ChatColor VARIABLE = ChatColor.YELLOW;
    public static final ChatColor DETAILS = ChatColor.GRAY;
    public static final String NO_PERMISSION_MESSAGE = "You don't have permission to do this!";
    public static final String INVALID_ARGUMENTS_MESSAGE = "Invalid arguments!";
    public static final String NO_EXECUTION_FROM_CONSOLE = "This command can not be executed from Console!";
    private final Server server;
    private final String prefix;
    private final PluginDescriptionFile description;

    public ChatUtil(PluginDescriptionFile description, ChatColor color, Server server) {
        this.description = description;
        this.prefix = color + description.getPrefix() + ChatColor.DARK_GRAY + " > ";
        this.server = server;
    }

    public void broadcast(String[] messages, boolean positive) {
        send(server.getConsoleSender(), messages, positive);
        send(server.getOnlinePlayers().toArray(new Player[server.getOnlinePlayers().size()]), messages, positive);
    }

    public void broadcast(String message, boolean positive) {
        send(server.getConsoleSender(), message, positive);
        send(server.getOnlinePlayers().toArray(new Player[server.getOnlinePlayers().size()]), message, positive);
    }

    public void adminBroadcast(String[] messages, boolean positive) {
        send(server.getConsoleSender(), messages, positive);
        send(getOperatorCommandSenders(), messages, positive);
    }

    public void adminBroadcast(String message, boolean positive) {
        send(server.getConsoleSender(), message, positive);
        send(getOperatorCommandSenders(), message, positive);
    }

    public void send(UUID[] receivers, String message, boolean positive) {
        for (UUID receiver : receivers) send(receiver, message, positive);
    }

    public void send(UUID[] receivers, String[] messages, boolean positive) {
        for (UUID receiver : receivers) send(receiver, messages, positive);
    }

    public void send(CommandSender[] receivers, String message, boolean positive) {
        for (CommandSender receiver : receivers) send(receiver, message, positive);
    }

    public void send(CommandSender[] receivers, String[] messages, boolean positive) {
        for (CommandSender receiver : receivers) send(receiver, messages, positive);
    }

    public void send(CommandSender receiver, String message, boolean positive) {
        receiver.sendMessage(prefix + (positive ? POSITIVE : NEGATIVE) + message);
    }

    public void send(CommandSender receiver, String[] messages, boolean positive) {
        for (String message : messages) {
            send(receiver, message, positive);
        }
    }

    public boolean send(UUID receiver, String[] messages, boolean positive) {
        CommandSender receiverCommandSender = server.getPlayer(receiver);
        if (receiverCommandSender != null) {
            send(receiverCommandSender, messages, positive);
            return true;
        }
        return false;
    }

    public boolean send(UUID receiver, String message, boolean positive) {
        CommandSender receiverCommandSender = server.getPlayer(receiver);
        if (receiverCommandSender != null) {
            send(receiverCommandSender, message, positive);
            return true;
        }
        return false;
    }

    //TODO XXX improve (calling this method is too expensive)
    public static String[] buildHelpMenu(String title, Map<String, String> commands) {
        String[] messages = new String[1 + (3 * commands.size())];
        messages[0] = "----- " + title + " | Help menu -----";
        int i = 1;
        for (String command : commands.keySet()) {
            messages[i++] = "";
            messages[i++] = VARIABLE + command;
            messages[i++] = DETAILS + commands.get(command);
        }
        return messages;
    }

    public static String[] buildHelpMenu(String title, String command, String description) {
        String[] messages = new String[4];
        messages[0] = "----- " + title + " | Help menu -----";
        messages[1] = "";
        messages[2] = VARIABLE + command;
        messages[3] = DETAILS + description;
        return messages;
    }

    public static String formatLocation(UnlinkedLocation location, boolean withWorld, boolean withDirection) {
        DecimalFormat df = new DecimalFormat("#0.000");
        return (withWorld ? location.getWorld() + " " : "")
                + "(" + df.format(location.getX()) + "|" + df.format(location.getY()) + "|" + df.format(location.getZ()) + ")"
                + (withDirection ? " [" + df.format(location.getYaw()) + "|" + df.format(location.getPitch()) + "]" : "");
    }

    public String[] buildAboutMenu() {
        String[] messages = new String[6];
        messages[0] = "----- " + description.getName() + " | About -----";
        messages[1] = "";
        messages[2] = DETAILS + description.getDescription();
        messages[3] = VARIABLE + "Version: " + DETAILS + description.getVersion();
        messages[4] = VARIABLE + "Website: " + DETAILS + description.getWebsite();
        String authors = description.getAuthors().get(0);
        for (int i = 1; i < description.getAuthors().size(); i++) authors += ", " + description.getAuthors().get(i);
        messages[5] = VARIABLE + "Authors: " + DETAILS + authors;
        return messages;
    }

    public String getPrefix() {
        return prefix;
    }

    private CommandSender[] getOperatorCommandSenders() {
        List<CommandSender> operatorCommandSenderList = new ArrayList<>();
        for (OfflinePlayer operatorOfflinePlayer : server.getOperators()) {
            Player operatorPlayer = operatorOfflinePlayer.getPlayer();
            if (operatorPlayer != null) operatorCommandSenderList.add(operatorPlayer);
        }
        return operatorCommandSenderList.toArray(new CommandSender[operatorCommandSenderList.size()]);
    }
}
