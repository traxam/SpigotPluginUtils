/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

/**
 * Used to create ItemStacks in a Factory class.
 * Created by tr808axm on 29.06.2016.
 */
public class ItemStackCreator {
    private final ItemStack item;

    public ItemStackCreator(Material type) {
        this(new ItemStack(type));
    }

    public ItemStackCreator(ItemStack item) {
        if (item == null) throw new IllegalArgumentException("item may not be null");
        this.item = item.clone();
    }

    public ItemStackCreator setType(Material type) {
        item.setType(type);
        return this;
    }

    public ItemStackCreator setDurability(short durability) {
        item.setDurability(durability);
        return this;
    }

    public ItemStackCreator addItemFlags(ItemFlag... itemFlags) {
        ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(itemFlags);
        item.setItemMeta(meta);
        return this;
    }

    public ItemStackCreator removeItemFlags(ItemFlag... itemFlags) {
        ItemMeta meta = item.getItemMeta();
        meta.removeItemFlags(itemFlags);
        item.setItemMeta(meta);
        return this;
    }

    public ItemStackCreator setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }

    public ItemStackCreator setName(String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return this;
    }

    public ItemStackCreator setLore(List<String> lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(lore);
        item.setItemMeta(meta);
        return this;
    }

    public ItemStackCreator setLore(String... lore) {
        return setLore(Arrays.asList(lore));
    }

    public ItemStackCreator addEnchantment(Enchantment enchantmentType, int level, boolean ignoreLevelRestriction) {
        ItemMeta meta = item.getItemMeta();
        meta.addEnchant(enchantmentType, level, ignoreLevelRestriction);
        item.setItemMeta(meta);
        return this;
    }

    public ItemStackCreator setItemMeta(ItemMeta meta) {
        item.setItemMeta(meta);
        return this;
    }

    public ItemStack getItemStack() {
        return item.clone();
    }
}
