/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Countdowns made simple.
 * Created by tr808axm on 28.05.2016.
 */
public class Countdown {
    public interface CountdownListener {
        void onCountdown(int countdownState);
    }

    private class CountdownRunnable implements Runnable {
        private int countdownState;

        public CountdownRunnable(int initialSeconds) {
            countdownState = initialSeconds;
        }

        @Override
        public synchronized void run() {
            countdownState--;
            callListeners(countdownState);
            if (countdownState == 0) stop();
        }

        public int getCountdownState() {
            return countdownState;
        }

        public synchronized void setCountdownState(int countdownState) {
            callListeners(countdownState);
            this.countdownState = countdownState;
        }
    }

    private final Plugin plugin;
    private final CountdownRunnable countdownRunnable;
    private final List<CountdownListener> countdownListeners;
    private boolean withExpDisplay;
    private Integer schedulerID;

    public Countdown(final int seconds, Plugin plugin, boolean withExpDisplay) {
        this.plugin = plugin;
        this.withExpDisplay = withExpDisplay;
        countdownListeners = new ArrayList<>();
        countdownRunnable = new CountdownRunnable(seconds);
        if (withExpDisplay) addListener(new CountdownListener() {
            @Override
            public void onCountdown(int countdownState) {
                updateExp(countdownState);
            }
        });
    }

    public boolean start() {
        if (schedulerID == null) {
            callListeners(countdownRunnable.countdownState);
            schedulerID = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, countdownRunnable, 20L, 20L);
            plugin.getLogger().info("Countdown with ID " + schedulerID + " started.");
            return true;
        }
        return false;
    }

    public boolean stop() {
        if (schedulerID != null) {
            plugin.getServer().getScheduler().cancelTask(schedulerID);
            if (withExpDisplay) updateExp(0);
            plugin.getLogger().info("Countdown with ID " + schedulerID + " stopped.");
            schedulerID = null;
            return true;
        }
        return false;
    }

    public void addListener(CountdownListener listener) {
        countdownListeners.add(listener);
    }

    public void removeListener(CountdownListener listener) {
        countdownListeners.remove(listener);
    }

    private void callListeners(int countdownState) {
        for (CountdownListener listener : countdownListeners)
            try {
                listener.onCountdown(countdownState);
            } catch (Exception e) {
                plugin.getLogger().severe("Exception in countdown listener call: ");
                e.printStackTrace();
            }
    }

    private void updateExp(int countdownState) {
        for (Player player : plugin.getServer().getOnlinePlayers()) player.setLevel(countdownState);
    }

    public boolean isRunning() {
        return schedulerID != null;
    }

    public boolean isWithExpDisplay() {
        return withExpDisplay;
    }

    public void setWithExpDisplay(boolean withExpDisplay) {
        this.withExpDisplay = withExpDisplay;
    }

    /**
     * @return 0 if the countdown has not been run yet, otherwise the last state the countdown had, even if it's stopped.
     */
    public int getLastCountdownState() {
        return countdownRunnable == null ? 0 : countdownRunnable.countdownState;
    }

    /**
     * Sets the countdown state to the given value.
     *
     * @param newCountDownState The value to set the countdown to.
     * @return true, if countdownRunnable is initialized.
     */
    public boolean setCountdownState(int newCountDownState) {
        if (countdownRunnable != null) {
            countdownRunnable.setCountdownState(newCountDownState);
            return true;
        }
        return false;
    }
}
