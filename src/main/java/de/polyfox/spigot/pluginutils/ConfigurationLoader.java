/*
 * Copyright (c) 2017 Polyfox
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.polyfox.spigot.pluginutils;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Used to load .yml configurations named different from config.yml.
 * Created by tr808axm on 13.05.2016.
 */
public class ConfigurationLoader {
    public interface ConfigurationReader {
        void readConfig(FileConfiguration fileConfiguration) throws Exception;
    }

    public interface ConfigurationWriter {
        void writeConfig(FileConfiguration fileConfiguration);
    }

    public interface InputStreamProvider {
        InputStream provideInputStream();
    }

    private final File configurationFile;
    private final FileConfiguration configuration;
    private final Logger logger;
    private final List<ConfigurationReader> configurationReaderList;
    private final List<ConfigurationWriter> configurationWriterList;

    public ConfigurationLoader(File configurationFile, Logger logger, InputStreamProvider defaultProvider) throws IOException {
        if (configurationFile.exists() && !configurationFile.isFile())
            throw new IllegalArgumentException("configurationFile isn't a file");
        this.logger = logger;
        this.configurationFile = configurationFile;
        this.configurationReaderList = new ArrayList<>();
        this.configurationWriterList = new ArrayList<>();

        if (!configurationFile.getParentFile().exists()) //noinspection ResultOfMethodCallIgnored
            configurationFile.getParentFile().mkdirs();
        if (!configurationFile.exists()) {
            logger.info(configurationFile.getName() + " not found, creating!");
            if (defaultProvider == null) {
                if (!configurationFile.createNewFile())
                    logger.warning(configurationFile.getName() + " could not be created.");
            } else {
                InputStream in = defaultProvider.provideInputStream();
                OutputStream out = new FileOutputStream(configurationFile);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = in.read(buffer)) > 0) out.write(buffer, 0, length);
                in.close();
                out.close();
            }
        }

        configuration = new YamlConfiguration();
    }

    @Deprecated
    public ConfigurationLoader(final Plugin plugin, final String configurationNameWithYmlExtension) throws IOException {
        this(new File(plugin.getDataFolder(), configurationNameWithYmlExtension), plugin.getLogger(), new InputStreamProvider() {
            @Override
            public InputStream provideInputStream() {
                return plugin.getResource(configurationNameWithYmlExtension);
            }
        });
    }

    public void save(boolean callConfigurationWriters, boolean callConfigurationReaders) throws Exception {
        if (callConfigurationWriters)
            callConfigurationWriters();
        try {
            configuration.save(configurationFile);
        } catch (IOException e) {
            logger.severe("Could not save " + configurationFile.getName() + ": " + e.getClass().getSimpleName());
            throw e;
        }
        logger.info("Successfully saved " + configurationFile.getName() + "!");
        reload(callConfigurationReaders);
    }

    public void reload(boolean callConfigurationReaders) throws Exception {
        try {
            configuration.load(configurationFile);
        } catch (IOException | InvalidConfigurationException e) {
            logger.severe("Could not load " + configurationFile.getName() + ": " + e.getClass().getSimpleName());
            throw e;
        }
        logger.info("Successfully loaded " + configurationFile.getName() + "!");
        if (callConfigurationReaders)
            callConfigurationReaders();
    }

    private void callConfigurationReaders() throws Exception {
        for (ConfigurationReader configurationReader : configurationReaderList)
            try {
                configurationReader.readConfig(configuration);
            } catch (Exception e) {
                logger.severe("Exception occurred while reading config (" + configurationReader.toString() + "): " + e.getClass().getSimpleName() + ": " + e.getMessage());
                throw e;
            }
    }

    private void callConfigurationWriters() {
        for (ConfigurationWriter configurationWriter : configurationWriterList)
            configurationWriter.writeConfig(configuration);
    }

    public void addConfigurationReader(ConfigurationReader configurationReader) {
        configurationReaderList.add(configurationReader);
    }

    public void addConfigurationWriter(ConfigurationWriter configurationWriter) {
        configurationWriterList.add(configurationWriter);
    }

    public void removeConfigurationReader(ConfigurationReader configurationReader) {
        configurationReaderList.remove(configurationReader);
    }

    public void removeConfigurationWriter(ConfigurationWriter configurationWriter) {
        configurationWriterList.remove(configurationWriter);
    }

    public File getConfigurationFile() {
        return configurationFile;
    }
}
